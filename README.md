# VSCode remote Docker-based rails development

This repo creates a Docker image suitable for use as a Ruby on Rails remote development environment that is compatible
with VSCode remote development (i.e. Remote Container: Reopen in Container).

## Build the base image

You can build the image locally via the command that follows but you can also just pull a pre-built image from the
Docker public repo.

```bash
prompt> docker build -t rails-dev-redis-vscode:latest -f dockerfiles/Dockerfile.dev .
```

## Copy devcontainer files

You will need to copy the `devcontainer` files into your project:

```bash
prompt> mkdir <YOUR_PROJECT>/.devcontainer
prompt> cp ./devcontainer/* <YOUR_PROJECT>/.devcontainer/
```

Obviously, `<YOUR_PROJECT>` is the directory location of your project!

With the above steps completed, you should be able to reopen your project in a remote container.

---
markeissler.org
